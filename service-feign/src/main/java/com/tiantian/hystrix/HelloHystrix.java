package com.tiantian.hystrix;

import com.tiantian.feign.EurekaClientFeign;
import org.springframework.stereotype.Component;

@Component
public class HelloHystrix implements EurekaClientFeign {
    @Override
    public String sayHello(String name) {
        return "sorry " + name + ",上游服务端口，服务降级";
    }
}
