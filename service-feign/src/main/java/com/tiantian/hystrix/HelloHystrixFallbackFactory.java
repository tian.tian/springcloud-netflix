package com.tiantian.hystrix;

import com.tiantian.feign.EurekaClientFeign;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

@Component
public class HelloHystrixFallbackFactory implements FallbackFactory<EurekaClientFeign> {

    @Override
    public EurekaClientFeign create(Throwable cause) {
        cause.printStackTrace();
        return new EurekaClientFeign() {
            @Override
            public String sayHello(String name) {
                return "sorry " + name + ",FallbackFactory上游服务端口，服务降级";
            }
        };
    }
}
