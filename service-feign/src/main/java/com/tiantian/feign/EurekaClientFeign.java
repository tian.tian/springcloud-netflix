package com.tiantian.feign;

import com.tiantian.hystrix.HelloHystrix;
import com.tiantian.hystrix.HelloHystrixFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "eureka-client", /*fallback = HelloHystrix.class*/ fallbackFactory = HelloHystrixFallbackFactory.class)
public interface EurekaClientFeign {

    @RequestMapping("/sayHello")
    String sayHello(@RequestParam(value = "name") String name);
}
