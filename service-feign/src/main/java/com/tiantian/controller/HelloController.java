package com.tiantian.controller;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.tiantian.feign.EurekaClientFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class HelloController {
    @Autowired
    RestTemplate restTemplate;
    @Autowired
    private EurekaClientFeign eurekaClientFeign;

    @RequestMapping("/hello")
    public String sayHello(){
        System.out.println("hello world !");
        String tiantian = eurekaClientFeign.sayHello("tiantian");
        return tiantian;
    }

    @RequestMapping("/test")
    @HystrixCommand(fallbackMethod = "callMethdError")
    public String test(){
        ResponseEntity<String> result = restTemplate.postForEntity("http://127.0.0.1:9090/sayHello", "test", String.class);
        System.out.println("hello world !");
        return result.getBody();
    }

    private String callMethdError(){

        return "haha";
    }
}
