package com.tiantian.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @RequestMapping("/sayHello")
    public String sayHello(String name) throws InterruptedException {
        System.out.println("hello world ! " + name);
//        Thread.sleep(20*1000);
        return "success";
    }
}
